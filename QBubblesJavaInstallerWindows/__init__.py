import os

import wx
from comtypes import CoInitialize
# noinspection PyProtectedMember
from wx._core import FontWeight

from QBubblesJavaInstallerWindows.lib import Downloader

JAVA_LINK = "https://github.com/ojdkbuild/ojdkbuild/releases/download/java-14-openjdk-14.0.2.12-1/java-14-openjdk-jre-14.0.2.12-1.windows.ojdkbuild.x86_64.msi"


class MainWindow(wx.Frame):
    def __init__(self, app):
        super(MainWindow, self).__init__(None, -1, size=(800, 560),
                                         style=wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.CLOSE_BOX))

        self.Bind(wx.EVT_CLOSE, lambda evt: os.kill(os.getpid(), 0))

        self.panel = wx.Panel(self, -1, size=(800, 520))

        # banner = tk.PhotoImage(file="downloader-icons/Banner.png")

        # canvas = tk.Canvas(root, height=60, bg="white")
        # canvas.pack(fill=tk.X)
        # canvas.create_image(0, 0, image=banner, anchor=tk.NW)

        self.verticalBox = wx.BoxSizer(wx.VERTICAL)

        # from base64 import b64decode
        # from zlib import decompress
        # image_data = decompress(b64decode(icon))
        #
        # from io import BytesIO
        # stream = BytesIO(bytearray(image_data))  # just bytes() for py3
        # image = wx.Image(stream, wx.BITMAP_TYPE_ANY)  # wx.ImageFromStream for legacy wx
        # self.reloadIcon = wx.Bitmap(image)  # wx.BitmapFromImage for legacy wx

        # # White background image.
        # self.png = wx.Image("downloader-icons/white.bmp", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        # wx.StaticBitmap(self, -1, self.png, (0, 0), (self.png.GetWidth(), self.png.GetHeight()))
        #
        # # White background image.
        # # self.png2 = wx.Image("downloader-icons/BannerNew.png", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        # # wx.StaticBitmap(self, -1, self.png, (0, 0), (self.png.GetWidth(), self.png.GetHeight()))
        #
        # # # Banner Image.
        # self.png2 = wx.Image("downloader-icons/BannerNew.jpg", wx.BITMAP_TYPE_ANY).ConvertToBitmap(16)
        # wx.StaticBitmap(self, 0, self.png2, (0, 0), (self.png2.GetWidth(), self.png2.GetHeight()))

        # Static line for below the images on the top.
        self.lineTop = wx.StaticLine(self.panel, -1, (0, 60), (800, 2))

        self.panel2 = wx.Panel(self.panel, -1, pos=(0, 62), size=(800, 428))

        self.panel2.SetBackgroundColour(wx.WHITE)

        self.title = wx.StaticText(self.panel2, -1, "Welcome to the QBubbles Installer!", pos=(15, 5), size=(790, 50))
        font = wx.Font(35, wx.FONTFAMILY_DECORATIVE, wx.FONTSTYLE_NORMAL, FontWeight(100))
        font.SetFaceName("Corbel Light")  # Yu Gothic UI Light")
        self.title.SetFont(font)
        self.title.SetForegroundColour("#003f7f")

        # Info label.
        self.label = wx.StaticText(self.panel2, -1, "Welcome to the QBubbles Installer!\r\n"
                                                    "This program will download Python, and the QBubbles launcher.\r\n"
                                                    "\r\n"
                                                    "If you want to change the behaviour of the downloading process, "
                                                    "do your thing here below.\r\n"
                                                    "If you don't know what the options are meant for, "
                                                    "press the 'Normal speed' button\r\n"
                                                    "Defaults are 16 threads, and 8192 for block size..\r\n"
                                                    "More the threads, the more speed with downloading.\r\n"
                                                    "WARNING: Too many threads can lead to crashing the internet, or"
                                                    "your computer.\r\n"
                                                    "\r\n"
                                                    "There's also an options for the block size.\r\n"
                                                    "Wich will be downloaded in one step.\r\n"
                                                    "Recommended value is 2048 Bytes.",
                                   pos=(15, 60), size=(790, 200), style=wx.ALIGN_LEFT)

        self.label.SetBackgroundColour(wx.WHITE)

        self.ignorePathChange = False

        ###################
        #     Threads     #
        ###################
        self.threadsLabel = wx.StaticText(self.panel2, -1, pos=(15, 275), size=(185, 15), label="Amount of threads:")
        self.threadsWidget = wx.SpinCtrl(self.panel2, -1, pos=(15, 295), size=(185, 23), min=1, max=128, initial=16)

        ######################
        #     Block Size     #
        ######################
        self.blockSizeLabel = wx.StaticText(self.panel2, -1, pos=(215, 275), size=(185, 15), label="Block Size:")
        self.blockSizeWidget = wx.SpinCtrl(self.panel2, -1, pos=(215, 295), size=(185, 23), min=1024, max=65536,
                                           initial=8192)

        ############################
        #     Quick Selections     #
        ############################
        self.quickSelections = wx.StaticText(self.panel2, -1, pos=(415, 275), size=(350, 15), label="Download speed:")

        # Normal speed.
        self.quickSelNormal = wx.Button(self.panel2, -1, label="Normal speed", pos=(414, 295), size=(119, 23))
        self.quickSelNormal.Bind(wx.EVT_BUTTON, lambda evt: self._speed_normal())

        # High speed.
        self.quickSelFastSpeed = wx.Button(self.panel2, -1, label="High Speed", pos=(532, 295), size=(119, 23))
        self.quickSelFastSpeed.Bind(wx.EVT_BUTTON, lambda evt: self._speed_high())

        # Ultra speed.
        self.quickSelFastSpeed = wx.Button(self.panel2, -1, label="Ultra Speed", pos=(650, 295), size=(119, 23))
        self.quickSelFastSpeed.Bind(wx.EVT_BUTTON, lambda evt: self._speed_ultra())

        # Info
        self.quickSelInfo = wx.StaticText(self.panel2, -1,
                                          label="WARING: Using high or ultra speed, can damage you pc!", pos=(415, 320))
        self.quickSelInfo.SetForegroundColour(wx.RED)

        ####################################################
        #     Static line for above the buttons below.     #
        ####################################################
        self.line = wx.StaticLine(self.panel, -1, (0, 490), (800, 2))

        ##########################
        #     Bottom buttons     #
        ##########################

        # Next / Download Button
        self.backBtn = wx.Button(self.panel, 0, "Back", (603, 492), (90, 28))
        self.backBtn.Bind(wx.EVT_BUTTON, lambda evt: None)
        self.backBtn.Disable()

        # Next / Download Button
        self.nextBtn = wx.Button(self.panel, 0, "Next", (693, 492), (90, 28))
        self.nextBtn.Bind(wx.EVT_BUTTON, lambda evt: self.change_panel(self.panel2, self.optionsPanel))
        self.nextBtn.SetDefault()
        # self.toFilePageBtn.Disable()

        #####################
        #     Set sizer     #
        #####################
        self.panel.SetSizer(self.verticalBox)

        # down.Bind(wx.EVT_BUTTON, download)

        # ------------------------------------------------------------------------------------------------------------ #
        # Page 2: File Save Page
        self.dlJavaPanel = wx.Panel(self, -1, size=(800, 428), pos=(0, 62))
        self.dlJavaPanel.SetBackgroundColour(wx.WHITE)

        self.dlJavaTitle = wx.StaticText(self.dlJavaPanel, -1, "Select install dlJava.", pos=(15, 5), size=(790, 50))
        self.dlJavaTitle.SetFont(font)
        self.dlJavaTitle.SetForegroundColour("#003f7f")

        # Info label.
        self.dlJavaLabel = wx.StaticText(self.dlJavaPanel, -1, "We are downloading Java 14.0.2 for you.\r\n",
                                          pos=(15, 60), size=(790, 100), style=wx.ALIGN_LEFT)
        self.javaProgressbar = wx.Gauge(self.dlJavaPanel, range=100, pos=(10, 220), size=(800, 24))
        self.dlJavaPanel.SetSizer(self.verticalBox)
        self.dlJavaPanel.Show(False)

        # ------------------------------------------------------------------------------------------------------------ #
        # Showing the main page.
        self.panel.Show(True)
        self.Update()

    def change_panel(self, from_, to_):
        CoInitialize()
        from_.Show(False)
        to_.Show(True)
        self.on_panel_change(to_)

    def on_panel_change(self, to):
        CoInitialize()
        if to == self.optionsPanel:
            self.backBtn.Unbind(wx.EVT_BUTTON)
            self.backBtn.Bind(wx.EVT_BUTTON, lambda evt: self.change_panel(self.optionsPanel, self.panel2))
            self.nextBtn.Unbind(wx.EVT_BUTTON)
            self.nextBtn.Bind(wx.EVT_BUTTON, lambda evt: None)
            self.backBtn.Enable()
            self.backBtn.SetDefault()
            self.nextBtn.Disable()
            self.downloadJava()
        if to == self.panel2:
            self.backBtn.Unbind(wx.EVT_BUTTON)
            self.backBtn.Bind(wx.EVT_BUTTON, lambda evt: None)
            self.nextBtn.Unbind(wx.EVT_BUTTON)
            self.nextBtn.Bind(wx.EVT_BUTTON, lambda evt: self.change_panel(self.panel2, self.optionsPanel))
            self.backBtn.Disable()
            self.nextBtn.Enable()
            self.nextBtn.SetDefault()

    def options_page(self):
        CoInitialize()
        self.panel2.Show(False)
        self.optionsPanel.Show(True)

    def _speed_normal(self):
        CoInitialize()
        self.blockSizeWidget.SetValue(8192)
        self.threadsWidget.SetValue(1)

    def _speed_high(self):
        CoInitialize()
        self.blockSizeWidget.SetValue(65536 / 2)
        self.threadsWidget.SetValue(64)

    def _speed_ultra(self):
        CoInitialize()
        self.blockSizeWidget.SetValue(65536)
        self.threadsWidget.SetValue(128)

    def downloadJava(self):
        CoInitialize()

        import random
        import os

        load = wx.ProgressDialog("Please Wait...", "")
        load.ShowModal(True)

        load.SetTitle("Downloading...")
        load.SetRange(100)
        load.Update(0, "Downloading...\n" + message)

        fp = "openjdk-jre-14.0.2.msi"
        url = JAVA_LINK

        value = random.randint(0x100000000000, 0xffffffffffff)
        if fp is None:
            filepath = hex(value)[2:] + ".tmp"
        else:
            filepath = fp

        if not os.path.exists("%s/temp" % os.getcwd().replace("\\", "/")):
            os.makedirs("%s/temp" % os.getcwd().replace("\\", "/"))

        download = Downloader(self).download_file(url, "%s/temp/%s" % (os.getcwd().replace("\\", "/"), filepath), self.threadsWidget.GetValue(), self.blockSizeWidget.GetValue())

        # Thread(None, download.download, "DownloadThread")

        load.SetRange(download.file_total_bytes + 1)
        while not download.downloaded:
            # print("Downloaded: ", download.file_downloaded_bytes)
            # print("Total: ", download.file_total_bytes)
            try:
                load.SetRange(download.file_total_bytes + 1)
                load.Update(download.file_downloaded_bytes, "Downloading...\n" + message)
            except wx._core.wxAssertionError:
                pass

        # load.Destroy()

        return "%s/temp/%s" % (os.getcwd().replace("\\", "/"), filepath)



if __name__ == '__main__':
    app = wx.App()
    MainWindow(app).Show()
    app.MainLoop()
