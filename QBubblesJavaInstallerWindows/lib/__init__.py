import math
import threading
import time
from typing import List

import requests
import wx
from comtypes import CoInitialize


class DownloadInfo(object):
    def __init__(self, size):
        self.complete = False
        self.size = size
        self.downloaded = 0


class MaxRetriesError(Exception):
    # noinspection PyUnusedName
    __module__ = "builtins"

    def __init__(self, *args):
        super(MaxRetriesError, self).__init__(*args)


class RequestException(Exception):
    def __init__(self, url: str, code: int):
        super().__init__(url, code)

        self.url = url
        self.code = code

    def __str__(self):
        return f"Url ‘{self.url}’ has an invalid status: {self.code}"


class InvalidUrlError(Exception):
    def __init__(self, *args, url):
        super().__init__(*args)
        self.url = url

    def __str__(self):
        args = "".join([str(arg) for arg in self.args])
        return args.replace("%s", self.url)


class Downloader(object):
    def __init__(self, mainwindow: wx.Frame) -> None:
        self.completedThreads = 0
        self.threads = []
        self.infoThreads: List[DownloadInfo] = []
        self.downloading: bool = True
        self.mainWindow = mainwindow
        self.downloadedBytes = 0
        self.fileSize = 0

    # The below code is used for each chunk of file handled
    # by each thread for downloading the content from specified
    # location to storage
    def handler(self, start, end, url, filename, thread_no, block_size):
        CoInitialize()  # Initialize thread in ‘c’

        # info_threads[thread_no]

        block_length = block_size
        downloaded = 0
        # open the file and write the content of the html page
        # into file.
        for index in range(start, end, block_length):
            error = True
            while error:
                try:
                    if index + block_length > end:
                        headers = {'Range': 'bytes=%d-%d' % (index, end)}
                        r = requests.get(url, headers=headers, stream=True)
                    else:
                        # specify the starting and ending of the file
                        headers = {'Range': 'bytes=%d-%d' % (index, index + block_length)}
                        r = requests.get(url, headers=headers, stream=True)
                    with open(filename, "r+b") as fp:
                        fp.seek(index)
                        var = fp.tell()
                        fp.write(r.content)
                        downloaded += block_length
                        self.infoThreads[thread_no].downloaded = downloaded
                    error = False
                except requests.ConnectionError:
                    error = True
        self.completedThreads += 1

    @staticmethod
    def _create_file(fp, size):
        with open(fp, "wb+") as file:
            file.seek(size - 1)
            file.write(b"\0")
            file.close()

    def download_file(self, url_of_file, name, number_of_threads, block_size):
        import re
        r = requests.head(url_of_file)
        if name:
            file_name = name
        else:
            try:
                d = r.headers['content-disposition']
                file_name = re.findall("filename=(.+)", d)[0]
            except KeyError:
                file_name = url_of_file.split('/')[-1]

        if r.status_code != 200:
            raise RequestException(url_of_file, r.status_code)

        max_retries = 30
        retries = max_retries

        while retries > 0:
            try:
                file_size = int(r.headers['content-length']) + 1
                self.fileSize = file_size
            except ValueError:
                raise InvalidUrlError("%s: Url have an non-valid ‘Content-Length’ header.", url=url_of_file)
            except KeyError:
                raise InvalidUrlError("%s: Url have an no ‘Content-Length’ header.", url=url_of_file)
            break

        if retries == 0:
            raise MaxRetriesError("", max_retries)

        part = int(self.fileSize / number_of_threads)

        self._create_file(name, self.fileSize)

        for i in range(number_of_threads):
            start = part * i
            end = start + part

            self.infoThreads.append(DownloadInfo(end - start))

            # create a Thread with start and end locations
            t = threading.Thread(target=self.handler,
                                 kwargs={'start': start, 'end': end - 1, 'url': url_of_file, 'filename': file_name,
                                         'thread_no': i, 'block_size': block_size})
            # t.setDaemon(True)
            t.start()

            self.threads.append(t)

        # Download loop.
        self.downloading = True
        while self.downloading:
            # Downloading temp variable.
            d24 = True

            # Check downloaded bytes.
            downloaded_bytes = 0
            for k in range(len(self.infoThreads)):
                downloaded_bytes += self.infoThreads[k].downloaded
            self.downloadedBytes = downloaded_bytes

            # Log test.
            print("Downloading of %s\n%s%% complete - %s of %s bytes downloaded.\nCompleted Threads: %s of %s" %
                  (file_name, 100 * downloaded_bytes / self.fileSize, downloaded_bytes, self.fileSize, self.completedThreads, number_of_threads))

            # Check completed thread to set the flag for downloading.
            if self.completedThreads == number_of_threads:
                d24 = False
            self.downloading = d24

            # 1 second delay.
            time.sleep(1)

        for t in self.threads:
            t.join()

        print('%s downloaded' % file_name)
